import argparse
import ovh
import requests

def get_options():
    parser = argparse.ArgumentParser()
    parser.add_argument('--endpoint', required=True)
    parser.add_argument('--application_key', required=True)
    parser.add_argument('--application_secret', required=True)
    parser.add_argument('--consumer_key', required=True)
    parser.add_argument('--zone', required=True)
    parser.add_argument('--fieldType', choices=['CNAME', 'TXT'], required=True)
    parser.add_argument('--subDomain', required=True)
    parser.add_argument('--target', required=True)
    return parser

args = get_options().parse_args()
client = ovh.Client(endpoint=args.endpoint, application_key=args.application_key, application_secret=args.application_secret, consumer_key=args.consumer_key)
if not client.get('/domain/zone/{zone}/record'.format(zone=args.zone), fieldType=args.fieldType, subDomain=args.subDomain):
    print(client.post('/domain/zone/{zone}/record'.format(zone=args.zone), fieldType=args.fieldType, subDomain=args.subDomain, target=args.target, ttl=None))
    client.post('/domain/zone/{zone}/refresh'.format(zone=args.zone))
else:
    print('already exists')
